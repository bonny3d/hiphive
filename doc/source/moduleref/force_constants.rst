Force constant models
=====================


.. index::
   single: Class reference; ForceConstantPotential

ForceConstantPotential
----------------------

.. autoclass:: hiphive.ForceConstantPotential
   :members:
   :undoc-members:



.. index::
   single: Class reference; ForceConstantCalculator

ForceConstantCalculator
-----------------------

.. autoclass:: hiphive.calculators.ForceConstantCalculator
   :members:
   :undoc-members:



.. index::
   single: Class reference; ForceConstants

ForceConstants
--------------

.. automodule:: hiphive.force_constants
   :members:



.. index::
   single: Rotational sum rules

Constraints
-----------

.. autofunction:: hiphive.enforce_rotational_sum_rules
   :noindex:
