.. _credits:
.. index:: Credits

.. |br| raw:: html

  <br/>


Credits
*******

:program:`hiphive` has been developed by `Fredrik Eriksson
<https://materialsmodeling.org/people/fredrik-eriksson/>`_, `Erik Fransson
<https://www.chalmers.se/en/staff/Pages/erikfr.aspx>`_, and `Paul Erhart
<https://materialsmodeling.org/people/paul-erhart/>`_ at the `Department of
Physics <https://www.chalmers.se/en/departments/physics/Pages/default.aspx>`_
of `Chalmers University of Technology <https://www.chalmers.se/>`_ in
Gothenburg, Sweden with funding from the Knut och Alice Wallenbergs Foundation,
the Swedish Research Council, the Swedish Foundation for Strategic Research,
and the Swedish National Infrastructure for Computing.

When using :program:`hiphive` in your research please cite the following paper:

* *The Hiphive Package for the Extraction of High‐Order Force Constants by Machine Learning* |br|
  Fredrik Eriksson, Erik Fransson, and Paul Erhart |br|
  Advanced Theory and Simulations, DOI:`10.1002/adts.201800184 <https://doi.org/10.1002/adts.201800184>`_ (2019)

:program:`hiphive` implements methods that have evolved in the field over many
years including work by, e.g.,

* Parlinski, Li, and Kawazoe [ParLiKaw97]_
* Esfarjani and Stokes [EsfSto08]_
* Hellman, Abrikosov, and Simak [HelAbrSim11]_
* Tadano, Gohda, and Tsuneyuki [TadGohTsu14]_
* Zhou, Nielson, Xia, and Ozolins [ZhoNieXia14]_
* Togo and Tanaka [TogTan15]_

Please cite these original papers as appropriate for your work.

For a general overview of the vibrational properties of materials see e.g., [Ful10]_.
